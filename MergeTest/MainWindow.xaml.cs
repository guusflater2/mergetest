﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MergeTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ToonBerichtButton_Click(object sender, RoutedEventArgs e)
        {
            BerichtTextBlock.Text = "Dit wordt een merge conflict.";
            MessageBox.Show("Dit wordt een merge conflict.");
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            BerichtButton.Content = "Klik me!";
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            BerichtButton.Content = "Toon Bericht";
        }
    }
}
